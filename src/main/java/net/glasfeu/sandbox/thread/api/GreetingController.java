package net.glasfeu.sandbox.thread.api;

import net.glasfeu.sandbox.thread.service.MyService;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.concurrent.CompletableFuture;

@RestController
public class GreetingController {

    private MyService myService;

    GreetingController(MyService myService) {
        this.myService = myService;
    }

    @GetMapping("/greeting")
    public String greeting(@RequestParam(name="name", required=false, defaultValue="World") String name, Model model) {
        model.addAttribute("name", name);
        return "greeting";
    }

    @GetMapping("/thread")
    public String thread(@RequestParam(name="name", required=false, defaultValue="World") String name, Model model) throws InterruptedException {
        model.addAttribute("name", name);

        System.out.println("Main process begin");
        CompletableFuture<String> future = myService.tryIt(name);

        future.thenAccept(value -> System.out.println("CompletableFuture1 : " + value));

        Runnable myTask = () -> System.out.println("Thread Lambda");
        myService.tryWithLambda(myTask);

        System.out.println("Main process end");

        return "greeting";
    }


}
