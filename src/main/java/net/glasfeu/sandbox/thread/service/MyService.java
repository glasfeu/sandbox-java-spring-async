package net.glasfeu.sandbox.thread.service;

import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.util.concurrent.CompletableFuture;

@Service
public class MyService {

    /*private TaskExecutor threadPoolExecutor;

    @Autowired
    MyService(@Qualifier("threadPoolExecutor") TaskExecutor threadPoolExecutor) {
        this.threadPoolExecutor = threadPoolExecutor.ex;
    }*/

    @Async("threadPoolExecutor")
    public CompletableFuture<String> tryIt(String usernameString) throws InterruptedException {

        Thread.sleep(1000L);

        System.out.println("Thread MyService: " + usernameString);

        return CompletableFuture.completedFuture("Le traitement de MyService");
    }


    @Async("threadPoolExecutor")
    public void tryWithLambda(Runnable task) throws InterruptedException {

        Thread.sleep(3000L);

        task.run();

    }


}
